ARG ALPINE_VERSION=latest
FROM alpine:${ALPINE_VERSION}

RUN apk add --no-cache buildah podman skopeo
    # && sed -i 's#^driver.*$#driver = "vfs"#g' /etc/containers/storage.conf